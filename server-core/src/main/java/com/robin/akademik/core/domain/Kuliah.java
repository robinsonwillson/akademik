package com.robin.akademik.core.domain;

import com.robin.akademik.core.domain.enumeration.Semester;

import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 * User: Robson
 * Date: 23/03/12
 * Time: 16:53
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name="kuliah")
public class Kuliah {
    @Id
    @Column(name="kodekuliah", length = 10)
    private String kodekuliah;

    @ManyToOne
    @JoinColumn(name="kodemk", nullable = false)
    private Matakuliah matakuliah;

    @ManyToOne
    @JoinColumn(name="nip", nullable = false)
    private Dosen dosen;

    @Column(name="tahunakademik", length = 10)
    private Semester tahunakademik;

    @Column(name="semester")
    private Semester semester;

    @Column(name="ruang", length = 100)
    private String ruang;

    @Column(name="jam", length = 15)
    private String jam;

    public String getKodekuliah() {
        return kodekuliah;
    }

    public void setKodekuliah(String kodekuliah) {
        this.kodekuliah = kodekuliah;
    }

    public Matakuliah getMatakuliah() {
        return matakuliah;
    }

    public void setMatakuliah(Matakuliah matakuliah) {
        this.matakuliah = matakuliah;
    }

    public Dosen getDosen() {
        return dosen;
    }

    public void setDosen(Dosen dosen) {
        this.dosen = dosen;
    }

    public String getRuang() {
        return ruang;
    }

    public void setRuang(String ruang) {
        this.ruang = ruang;
    }

    public String getJam() {
        return jam;
    }

    public void setJam(String jam) {
        this.jam = jam;
    }

    public Semester getTahunakademik() {
        return tahunakademik;
    }

    public void setTahunakademik(Semester tahunakademik) {
        this.tahunakademik = tahunakademik;
    }

    public Semester getSemester() {
        return semester;
    }

    public void setSemester(Semester semester) {
        this.semester = semester;
    }
}
