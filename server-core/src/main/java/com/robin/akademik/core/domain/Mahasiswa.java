package com.robin.akademik.core.domain;

import com.robin.akademik.core.domain.enumeration.StatusMahasiswa;

import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 * User: Robson
 * Date: 23/03/12
 * Time: 16:40
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name="mahasiswa")
public class Mahasiswa {
    @Id
    @Column(name="nim", length = 6)
    private String nim;

    @Column(name="nama", length = 100)
    private String nama;

    @Column(name="alamat", length = 100)
    private String alamat;

    @ManyToOne
    @JoinColumn(name="kodejurusan", nullable = false)
    private Jurusan jurusan;

    @ManyToOne
    @JoinColumn(name="namapropinsi", nullable = false)
    private Propinsi propinsi;

    @Column(name="status")
    private StatusMahasiswa status;

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public Jurusan getJurusan() {
        return jurusan;
    }

    public void setJurusan(Jurusan jurusan) {
        this.jurusan = jurusan;
    }

    public Propinsi getPropinsi() {
        return propinsi;
    }

    public void setPropinsi(Propinsi propinsi) {
        this.propinsi = propinsi;
    }

    public StatusMahasiswa getStatus() {
        return status;
    }

    public void setStatus(StatusMahasiswa status) {
        this.status = status;
    }
}
