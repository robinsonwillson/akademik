package com.robin.akademik.core.domain;

import com.robin.akademik.core.domain.enumeration.Nilai;

import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 * User: Robson
 * Date: 23/03/12
 * Time: 17:02
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name = "peserta")
public class Peserta {

    @Id
    private String id;

    @ManyToOne
    @JoinColumn(name = "kodekuliah", nullable = false)
    private Kuliah kuliah;

    @ManyToOne
    @JoinColumn(name = "nim", nullable = false)
    private Mahasiswa mahasiswa;

    @JoinColumn(name = "nilai")
    private Nilai nilai;

    public Kuliah getKuliah() {
        return kuliah;
    }

    public void setKuliah(Kuliah kuliah) {
        this.kuliah = kuliah;
    }

    public Mahasiswa getMahasiswa() {
        return mahasiswa;
    }

    public void setMahasiswa(Mahasiswa mahasiswa) {
        this.mahasiswa = mahasiswa;
    }

    public Nilai getNilai() {
        return nilai;
    }

    public void setNilai(Nilai nilai) {
        this.nilai = nilai;
    }
}
