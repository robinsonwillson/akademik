package com.robin.akademik.core.domain;

/**
 * Created by IntelliJ IDEA.
 * User: Robson
 * Date: 23/03/12
 * Time: 16:03
 * To change this template use File | Settings | File Templates.
 */

import org.hibernate.annotations.CollectionId;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="dosen")
public class Dosen {
    
    @Id
    @Column(name="nip", length = 18)
    private String nip;

    @Column(name="nama", length = 100)
    private String nama;

    @Column(name="alamat", length = 100)
    private String alamat;

    @Column(name="telepon", nullable = true, length = 50)
    private String telepon;

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }
}
