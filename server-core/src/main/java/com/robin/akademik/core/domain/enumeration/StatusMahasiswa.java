package com.robin.akademik.core.domain.enumeration;

/**
 * Created by IntelliJ IDEA.
 * User: Robson
 * Date: 23/03/12
 * Time: 16:44
 * To change this template use File | Settings | File Templates.
 */
public enum StatusMahasiswa {
    AKTIF, CUTI, DO, LULUS;
}
