package com.robin.akademik.core.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by IntelliJ IDEA.
 * User: Robson
 * Date: 23/03/12
 * Time: 16:06
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name="fakultas")
public class Fakultas {

    @Id
    @Column(name="fakultas", length = 50)
    private String fakultas;

    public String getFakultas() {
        return fakultas;
    }

    public void setFakultas(String fakultas) {
        this.fakultas = fakultas;
    }
}
