package com.robin.akademik.core.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by IntelliJ IDEA.
 * User: Robson
 * Date: 23/03/12
 * Time: 16:04
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name="jurusan")
public class Jurusan {
    @Id
    @Column(name="kodejurusan", length = 5)
    private String kodejurusan;

    @Column(name="namajurusan", length = 100)
    private String namajurusan;

    public String getKodeJurusan() {
        return kodejurusan;
    }

    public void setKodeJurusan(String kodeJurusan) {
        this.kodejurusan = kodeJurusan;
    }

    public String getNamaJurusan() {
        return namajurusan;
    }

    public void setNamaJurusan(String namaJurusan) {
        this.namajurusan = namaJurusan;
    }
}
