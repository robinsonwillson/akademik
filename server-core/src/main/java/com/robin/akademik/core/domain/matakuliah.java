package com.robin.akademik.core.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by IntelliJ IDEA.
 * User: Robson
 * Date: 23/03/12
 * Time: 16:03
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name="matakuliah")
public class Matakuliah {

    @Id
    @Column(name="kodemk", length = 5)
    private String kodemk;

    @Column(name="namamk", length = 100)
    private String namamk;

    @Column(name="sks", length = 2)
    private int sks;

    public String getKodemk() {
        return kodemk;
    }

    public void setKodemk(String kodemk) {
        this.kodemk = kodemk;
    }

    public String getNamamk() {
        return namamk;
    }

    public void setNamamk(String namamk) {
        this.namamk = namamk;
    }

    public int getSks() {
        return sks;
    }

    public void setSks(int sks) {
        this.sks = sks;
    }
}
