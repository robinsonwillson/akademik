package com.robin.akademik.core.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by IntelliJ IDEA.
 * User: Robson
 * Date: 23/03/12
 * Time: 16:04
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name="propinsi")
public class Propinsi {
    @Id
    @Column(name="namaPropinsi", length = 50)
    private String namaPropinsi;

    public String getNamaPropinsi() {
        return namaPropinsi;
    }

    public void setNamaPropinsi(String namaPropinsi) {
        this.namaPropinsi = namaPropinsi;
    }
}
