package com.robin.akademik.core.editor;

import com.robin.akademik.core.domain.Dosen;
import com.robin.akademik.core.service.DosenService;

import java.beans.PropertyEditorSupport;

/**
 * Created by IntelliJ IDEA.
 * User: robinson
 * Date: 3/26/12
 * Time: 9:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class DosenEditor extends PropertyEditorSupport {

    private DosenService dosenService;

    public DosenEditor(DosenService dosenService) {
        this.dosenService = dosenService;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        Dosen dosen = dosenService.find(text);

        if (dosen != null) {
            setValue(dosen);
        } else {
            throw new IllegalArgumentException("Dosen with NIP " + text + " not Found!");
        }
    }

    @Override
    public String getAsText() {
        if (getValue() != null) {
            Dosen dosen = (Dosen) getValue();
            return dosen.getNip() + "";
        } else {
            return "";
        }
    }
}
