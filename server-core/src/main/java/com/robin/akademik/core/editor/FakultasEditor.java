package com.robin.akademik.core.editor;

import com.robin.akademik.core.domain.Fakultas;
import com.robin.akademik.core.service.FakultasService;

import java.beans.PropertyEditorSupport;

/**
 * Created by IntelliJ IDEA.
 * User: robinson
 * Date: 3/26/12
 * Time: 9:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class FakultasEditor extends PropertyEditorSupport {

    private FakultasService fakultasService;

    public FakultasEditor(FakultasService fakultasService) {
        this.fakultasService = fakultasService;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        Fakultas fakultas = fakultasService.find(text);

        if (fakultas != null) {
            setValue(fakultas);
        } else {
            throw new IllegalArgumentException("Fakultas with Name " + text + " not Found!");
        }
    }

    @Override
    public String getAsText() {
        if (getValue() != null) {
            Fakultas fakultas = (Fakultas) getValue();
            return fakultas.getFakultas() + "";
        } else {
            return "";
        }
    }
}
