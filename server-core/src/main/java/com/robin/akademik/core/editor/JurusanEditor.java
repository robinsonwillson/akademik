package com.robin.akademik.core.editor;

import com.robin.akademik.core.domain.Jurusan;
import com.robin.akademik.core.service.JurusanService;

import java.beans.PropertyEditorSupport;

/**
 * Created by IntelliJ IDEA.
 * User: robinson
 * Date: 3/26/12
 * Time: 9:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class JurusanEditor extends PropertyEditorSupport {

    private JurusanService jurusanService;

    public JurusanEditor(JurusanService jurusanService) {
        this.jurusanService = jurusanService;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        Jurusan jurusan = jurusanService.find(text);

        if (jurusan != null) {
            setValue(jurusan);
        } else {
            throw new IllegalArgumentException("Jurusan with Kode Jurusan " + text + " not Found!");
        }
    }

    @Override
    public String getAsText() {
        if (getValue() != null) {
            Jurusan jurusan = (Jurusan) getValue();
            return jurusan.getKodeJurusan() + "";
        } else {
            return "";
        }
    }
}
