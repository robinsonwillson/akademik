package com.robin.akademik.core.editor;

import com.robin.akademik.core.domain.Kuliah;
import com.robin.akademik.core.service.KuliahService;

import java.beans.PropertyEditorSupport;

/**
 * Created by IntelliJ IDEA.
 * User: robinson
 * Date: 3/26/12
 * Time: 9:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class KuliahEditor extends PropertyEditorSupport {

    private KuliahService kuliahService;

    public KuliahEditor(KuliahService kuliahService) {
        this.kuliahService = kuliahService;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        Kuliah kuliah = kuliahService.find(text);

        if (kuliah != null) {
            setValue(kuliah);
        } else {
            throw new IllegalArgumentException("Kuliah with kode " + text + " not Found!");
        }
    }

    @Override
    public String getAsText() {
        if (getValue() != null) {
            Kuliah kuliah = (Kuliah) getValue();
            return kuliah.getKodekuliah() + "";
        } else {
            return "";
        }
    }
}
