package com.robin.akademik.core.editor;

import com.robin.akademik.core.domain.Mahasiswa;
import com.robin.akademik.core.service.MahasiswaService;

import java.beans.PropertyEditorSupport;

/**
 * Created by IntelliJ IDEA.
 * User: robinson
 * Date: 3/26/12
 * Time: 9:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class MahasiswaEditor extends PropertyEditorSupport {

    private MahasiswaService mahasiswaService;

    public MahasiswaEditor(MahasiswaService mahasiswaService) {
        this.mahasiswaService = mahasiswaService;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        Mahasiswa mahasiswa = mahasiswaService.find(text);

        if (mahasiswa != null) {
            setValue(mahasiswa);
        } else {
            throw new IllegalArgumentException("Mahasiswa with NIM " + text + " not Found!");
        }
    }

    @Override
    public String getAsText() {
        if (getValue() != null) {
            Mahasiswa mahasiswa = (Mahasiswa) getValue();
            return mahasiswa.getNim() + "";
        } else {
            return "";
        }
    }
}
