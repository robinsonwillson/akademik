package com.robin.akademik.core.editor;

import com.robin.akademik.core.domain.Matakuliah;
import com.robin.akademik.core.service.MatakuliahService;

import java.beans.PropertyEditorSupport;

/**
 * Created by IntelliJ IDEA.
 * User: robinson
 * Date: 3/26/12
 * Time: 9:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class MatakuliahEditor extends PropertyEditorSupport {

    private MatakuliahService matakuliahService;

    public MatakuliahEditor(MatakuliahService matakuliahService) {
        this.matakuliahService = matakuliahService;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        Matakuliah matakuliah = matakuliahService.find(text);

        if (matakuliah != null) {
            setValue(matakuliah);
        } else {
            throw new IllegalArgumentException("Matakuliah with Kode MK " + text + " not Found!");
        }
    }

    @Override
    public String getAsText() {
        if (getValue() != null) {
            Matakuliah matakuliah = (Matakuliah) getValue();
            return matakuliah.getKodemk() + "";
        } else {
            return "";
        }
    }
}
