package com.robin.akademik.core.editor;

import com.robin.akademik.core.domain.Peserta;
import com.robin.akademik.core.service.PesertaService;

import java.beans.PropertyEditorSupport;

/**
 * Created by IntelliJ IDEA.
 * User: robinson
 * Date: 3/26/12
 * Time: 9:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class PesertaEditor extends PropertyEditorSupport {

    private PesertaService pesertaService;

    public PesertaEditor(PesertaService pesertaService) {
        this.pesertaService = pesertaService;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        Peserta peserta = pesertaService.find(text);

        if (peserta != null) {
            setValue(peserta);
        } else {
            throw new IllegalArgumentException("Peserta with  " + text + " not Found!");
        }
    }

    @Override
    public String getAsText() {
        if (getValue() != null) {
            Peserta peserta = (Peserta) getValue();
            return peserta.getMahasiswa() + "";
        } else {
            return "";
        }
    }
}
