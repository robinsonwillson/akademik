package com.robin.akademik.core.editor;

import com.robin.akademik.core.domain.Propinsi;
import com.robin.akademik.core.service.PropinsiService;

import java.beans.PropertyEditorSupport;

/**
 * Created by IntelliJ IDEA.
 * User: robinson
 * Date: 3/26/12
 * Time: 9:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class PropinsiEditor extends PropertyEditorSupport {

    private PropinsiService propinsiService;

    public PropinsiEditor(PropinsiService propinsiService) {
        this.propinsiService = propinsiService;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        Propinsi propinsi = propinsiService.find(text);

        if (propinsi != null) {
            setValue(propinsi);
        } else {
            throw new IllegalArgumentException("Propinsi with Name " + text + " not Found!");
        }
    }

    @Override
    public String getAsText() {
        if (getValue() != null) {
            Propinsi propinsi = (Propinsi) getValue();
            return propinsi.getNamaPropinsi() + "";
        } else {
            return "";
        }
    }
}
