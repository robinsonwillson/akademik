package com.robin.akademik.core.service;

import com.robin.akademik.core.domain.Dosen;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 7:02 PM
 * To change this template use File | Settings | File Templates.
 */
public interface DosenService {

    public void save(Dosen dosen);

    public void delete(Dosen dosen);

    public void deletes(Dosen[] dosens);

    public Dosen find(String id);

    public List<Dosen> find();

    public Long count();
}
