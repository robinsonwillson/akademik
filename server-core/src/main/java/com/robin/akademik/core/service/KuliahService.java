package com.robin.akademik.core.service;

import com.robin.akademik.core.domain.Kuliah;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 7:02 PM
 * To change this template use File | Settings | File Templates.
 */
public interface KuliahService {
    public void save(Kuliah kuliah) ;
    public void delete(Kuliah kuliah);

    public void deletes(Kuliah[] kuliahs) ;
    public Kuliah find(String id) ;
    public List<Kuliah> find() ;

    public Long count() ;
}
