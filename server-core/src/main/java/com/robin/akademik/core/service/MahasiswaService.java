package com.robin.akademik.core.service;

import com.robin.akademik.core.domain.Mahasiswa;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 7:02 PM
 * To change this template use File | Settings | File Templates.
 */

public interface MahasiswaService {

    public void save(Mahasiswa mahasiswa) ;
    public void delete(Mahasiswa mahasiswa);
    public void deletes(Mahasiswa[] mahasiswas);
    public Mahasiswa find(String id) ;
    public List<Mahasiswa> find() ;
    public Long count() ;
}
