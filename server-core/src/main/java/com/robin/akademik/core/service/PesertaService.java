package com.robin.akademik.core.service;

import com.robin.akademik.core.domain.Peserta;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 7:02 PM
 * To change this template use File | Settings | File Templates.
 */
public interface PesertaService {
    public void save(Peserta peserta) ;
    public void delete(Peserta peserta);
    public void deletes(Peserta[] pesertas);
    public Peserta find(String id) ;
    public List<Peserta> find() ;
    public Long count() ;
}
