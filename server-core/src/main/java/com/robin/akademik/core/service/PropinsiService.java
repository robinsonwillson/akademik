package com.robin.akademik.core.service;

import com.robin.akademik.core.domain.Propinsi;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 7:02 PM
 * To change this template use File | Settings | File Templates.
 */
public interface PropinsiService {
    

    public void save(Propinsi propinsi) ;

    public void delete(Propinsi propinsi) ;

    public void deletes(Propinsi[] propinsis) ;

    public Propinsi find(String id) ;

    public List<Propinsi> find() ;

    public Long count() ;
}
