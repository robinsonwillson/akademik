package com.robin.akademik.core.service.impl;

import com.robin.akademik.core.domain.Dosen;
import com.robin.akademik.core.service.DosenService;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 7:02 PM
 * To change this template use File | Settings | File Templates.
 */
@Transactional
//@Service("dosenService")
public class DosenServiceImpl implements DosenService {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    public void save(Dosen dosen) {
        sessionFactory.getCurrentSession().saveOrUpdate(dosen);
    }

    public void delete(Dosen dosen) {
        sessionFactory.getCurrentSession().delete(dosen);
    }

    public void deletes(Dosen[] dosens) {
        for(Dosen dosen:dosens){
            delete(dosen);
        }
    }

    public Dosen find(String id) {
        return (Dosen) sessionFactory.getCurrentSession().get(Dosen.class, id);
    }

    public List<Dosen> find() {
        return (List<Dosen>) sessionFactory.getCurrentSession().createQuery(
                "SELECT dosen from Dosen dosen ")
                .list();
    }

    public Long count() {
        return (Long) sessionFactory.getCurrentSession().createQuery(
                "SELECT count(dosen) from Dosen dosen ")
                .uniqueResult();
    }
}
