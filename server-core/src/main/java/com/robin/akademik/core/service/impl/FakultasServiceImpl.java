package com.robin.akademik.core.service.impl;

import com.robin.akademik.core.domain.Fakultas;
import com.robin.akademik.core.service.FakultasService;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 7:02 PM
 * To change this template use File | Settings | File Templates.
 */
@Transactional
//@Service("fakultasService")
public class FakultasServiceImpl implements FakultasService{
    
    @Autowired
    private SessionFactory sessionFactory;
    
    public void save(Fakultas fakultas) {
        sessionFactory.getCurrentSession().saveOrUpdate(fakultas);
    }

    public void delete(Fakultas fakultas) {
        sessionFactory.getCurrentSession().delete(fakultas);
    }

    public void deletes(Fakultas[] fakultass) {
        for(Fakultas fakultas:fakultass){
            delete(fakultas);
        }
    }

    public Fakultas find(String id) {
        return (Fakultas) sessionFactory.getCurrentSession().get(Fakultas.class, id);
    }

    public List<Fakultas> find() {
        return (List<Fakultas>) sessionFactory.getCurrentSession().createQuery(
                "SELECT fakultas from Fakultas fakultas ")
                .list();
    }

    public Long count() {
        return (Long) sessionFactory.getCurrentSession().createQuery(
                "SELECT count(fakultas) from Fakultas fakultas ")
                .uniqueResult();
    }
}
