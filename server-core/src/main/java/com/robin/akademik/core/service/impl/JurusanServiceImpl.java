package com.robin.akademik.core.service.impl;

import com.robin.akademik.core.domain.Jurusan;
import com.robin.akademik.core.service.JurusanService;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 7:02 PM
 * To change this template use File | Settings | File Templates.
 */
@Transactional
//@Service("jurusanService")
public class JurusanServiceImpl implements JurusanService{
    
    @Autowired
    private SessionFactory sessionFactory;
    
    public void save(Jurusan jurusan) {
        sessionFactory.getCurrentSession().saveOrUpdate(jurusan);
    }

    public void delete(Jurusan jurusan) {
        sessionFactory.getCurrentSession().delete(jurusan);
    }

    public void deletes(Jurusan[] jurusans) {
        for(Jurusan jurusan:jurusans){
            delete(jurusan);
        }
    }

    public Jurusan find(String id) {
        return (Jurusan) sessionFactory.getCurrentSession().get(Jurusan.class, id);
    }

    public List<Jurusan> find() {
        return (List<Jurusan>) sessionFactory.getCurrentSession().createQuery(
                "SELECT jurusan from Jurusan jurusan ")
                .list();
    }

    public Long count() {
        return (Long) sessionFactory.getCurrentSession().createQuery(
                "SELECT count(jurusan) from Jurusan jurusan ")
                .uniqueResult();
    }
}
