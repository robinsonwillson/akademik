package com.robin.akademik.core.service.impl;

import com.robin.akademik.core.domain.Kuliah;
import com.robin.akademik.core.service.KuliahService;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 7:02 PM
 * To change this template use File | Settings | File Templates.
 */
@Transactional
//@Service("kuliahService")
public class KuliahServiceImpl implements KuliahService{
    
    @Autowired
    private SessionFactory sessionFactory;
    
    public void save(Kuliah kuliah) {
        sessionFactory.getCurrentSession().saveOrUpdate(kuliah);
    }

    public void delete(Kuliah kuliah) {
        sessionFactory.getCurrentSession().delete(kuliah);
    }

    public void deletes(Kuliah[] kuliahs) {
        for(Kuliah kuliah:kuliahs){
            delete(kuliah);
        }
    }

    public Kuliah find(String id) {
        return (Kuliah) sessionFactory.getCurrentSession().get(Kuliah.class, id);
    }

    public List<Kuliah> find() {
        return (List<Kuliah>) sessionFactory.getCurrentSession().createQuery(
                "SELECT kuliah from Kuliah kuliah ")
                .list();
    }

    public Long count() {
        return (Long) sessionFactory.getCurrentSession().createQuery(
                "SELECT count(kuliah) from Kuliah kuliah ")
                .uniqueResult();
    }
}
