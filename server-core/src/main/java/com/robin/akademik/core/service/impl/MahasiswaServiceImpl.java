package com.robin.akademik.core.service.impl;

import com.robin.akademik.core.domain.Mahasiswa;
import com.robin.akademik.core.service.MahasiswaService;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 7:02 PM
 * To change this template use File | Settings | File Templates.
 */
@Transactional
//@Service("mahasiswaService")
public class MahasiswaServiceImpl implements MahasiswaService{

    @Autowired
    private SessionFactory sessionFactory;

    public void save(Mahasiswa mahasiswa) {
        sessionFactory.getCurrentSession().saveOrUpdate(mahasiswa);
    }

    public void delete(Mahasiswa mahasiswa) {
        sessionFactory.getCurrentSession().delete(mahasiswa);
    }

    public void deletes(Mahasiswa[] mahasiswas) {
        for(Mahasiswa mahasiswa:mahasiswas){
            delete(mahasiswa);
        }
    }

    public Mahasiswa find(String id) {
        return (Mahasiswa) sessionFactory.getCurrentSession().get(Mahasiswa.class, id);
    }

    public List<Mahasiswa> find() {
        return (List<Mahasiswa>) sessionFactory.getCurrentSession().createQuery(
                "SELECT mahasiswa from Mahasiswa mahasiswa ")
                .list();
    }

    public Long count() {
        return (Long) sessionFactory.getCurrentSession().createQuery(
                "SELECT count(mahasiswa) from Mahasiswa mahasiswa ")
                .uniqueResult();
    }
}
