package com.robin.akademik.core.service.impl;

import com.robin.akademik.core.domain.Matakuliah;
import com.robin.akademik.core.service.MatakuliahService;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 7:02 PM
 * To change this template use File | Settings | File Templates.
 */
@Transactional
//@Service("matakuliahService")
public class MatakuliahServiceImpl implements MatakuliahService{
    
    @Autowired
    private SessionFactory sessionFactory;
    
    public void save(Matakuliah matakuliah) {
        sessionFactory.getCurrentSession().saveOrUpdate(matakuliah);
    }

    public void delete(Matakuliah matakuliah) {
        sessionFactory.getCurrentSession().delete(matakuliah);
    }

    public void deletes(Matakuliah[] matakuliahs) {
        for(Matakuliah matakuliah:matakuliahs){
            delete(matakuliah);
        }
    }

    public Matakuliah find(String id) {
        return (Matakuliah) sessionFactory.getCurrentSession().get(Matakuliah.class, id);
    }

    public List<Matakuliah> find() {
        return (List<Matakuliah>) sessionFactory.getCurrentSession().createQuery(
                "SELECT matakuliah from Matakuliah matakuliah ")
                .list();
    }

    public Long count() {
        return (Long) sessionFactory.getCurrentSession().createQuery(
                "SELECT count(matakuliah) from Matakuliah matakuliah ")
                .uniqueResult();
    }
}
