package com.robin.akademik.core.service.impl;

import com.robin.akademik.core.domain.Peserta;
import com.robin.akademik.core.service.PesertaService;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 7:02 PM
 * To change this template use File | Settings | File Templates.
 */
@Transactional
//@Service("pesertaService")
public class PesertaServiceImpl implements PesertaService{
    
    @Autowired
    private SessionFactory sessionFactory;
    
    public void save(Peserta peserta) {
        sessionFactory.getCurrentSession().saveOrUpdate(peserta);
    }

    public void delete(Peserta peserta) {
        sessionFactory.getCurrentSession().delete(peserta);
    }

    public void deletes(Peserta[] pesertas) {
        for(Peserta peserta:pesertas){
            delete(peserta);
        }
    }

    public Peserta find(String id) {
        return (Peserta) sessionFactory.getCurrentSession().get(Peserta.class, id);
    }

    public List<Peserta> find() {
        return (List<Peserta>) sessionFactory.getCurrentSession().createQuery(
                "SELECT peserta from Peserta peserta ")
                .list();
    }

    public Long count() {
        return (Long) sessionFactory.getCurrentSession().createQuery(
                "SELECT count(peserta) from Peserta peserta ")
                .uniqueResult();
    }
}
