package com.robin.akademik.core.service.impl;

import com.robin.akademik.core.domain.Propinsi;
import com.robin.akademik.core.service.PropinsiService;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 7:02 PM
 * To change this template use File | Settings | File Templates.
 */
@Transactional
//@Service("propinsiService")
public class PropinsiServiceImpl implements PropinsiService{
    
    @Autowired
    protected SessionFactory sessionFactory;
    
    public void save(Propinsi propinsi) {
        sessionFactory.getCurrentSession().saveOrUpdate(propinsi);
    }

    public void delete(Propinsi propinsi) {
        sessionFactory.getCurrentSession().delete(propinsi);
    }

    public void deletes(Propinsi[] propinsis) {
        for(Propinsi propinsi:propinsis){
            delete(propinsi);
        }
    }

    public Propinsi find(String id) {
        return (Propinsi) sessionFactory.getCurrentSession().get(Propinsi.class, id);
    }

    public List<Propinsi> find() {
        return (List<Propinsi>) sessionFactory.getCurrentSession().createQuery(
                "SELECT propinsi from Propinsi propinsi ")
                .list();
    }

    public Long count() {
        return (Long) sessionFactory.getCurrentSession().createQuery(
                "SELECT count(propinsi) from Propinsi propinsi ")
                .uniqueResult();
    }
}
