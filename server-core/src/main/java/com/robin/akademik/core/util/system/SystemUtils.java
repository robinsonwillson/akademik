package com.robin.akademik.core.util.system;

/**
 * Created by IntelliJ IDEA.
 * User: Robson
 * Date: 03/04/12
 * Time: 18:24
 * To change this template use File | Settings | File Templates.
 */
public final class  SystemUtils {
    public final static String HOST_FAKULTAS = "http://localhost:8081/fakultas/";
    public final static String HOST_WS_FAKULTAS = "http://localhost:8081/fakultas/rest";
    public final static String HOST_UNIVERSITAS = "http://localhost:8080/universitas/";
    public final static String HOST_WS_UNIVERSITAS = "http://localhost:8080/universitas/rest";
}
