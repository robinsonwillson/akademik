package com.robin.akademik.mahasiswa.controller;

import com.robin.akademik.core.domain.Mahasiswa;
import com.robin.akademik.core.editor.MahasiswaEditor;
import com.robin.akademik.core.service.MahasiswaService;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by IntelliJ IDEA.
 * User: Robson
 * Date: 27/03/12
 * Time: 16:39
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "mahasiswa")
public class MahasiswaController {

    @Autowired
    @Qualifier("mahasiswaFakultasService")
    private MahasiswaService mahasiswaService;

    /**
     * Ini select * all
     * @param modelMap
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public String find(ModelMap modelMap) {
        modelMap.addAttribute("mahasiswa", mahasiswaService.find());
        return "mahasiswa/list";
    }

    @RequestMapping(params = "nip", method = RequestMethod.GET)
    public String find(ModelMap modelMap, @RequestParam("nip") Mahasiswa mahasiswa) {
        modelMap.addAttribute("event", "DETAIL");
        modelMap.addAttribute("mahasiswa", mahasiswa);
        return "mahasiswa/input";
    }

    /**
     * kembalikan form inputan
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "new", method = RequestMethod.GET)
    public String create(ModelMap modelMap) {
        modelMap.addAttribute("event", "INPUT");
        modelMap.addAttribute("httpMethod", "POST");
        modelMap.addAttribute("mahasiswa", new Mahasiswa());
        return "mahasiswa/input";
    }

    /***
     * Simpan hasil inputan
     * @param mahasiswa
     * @param bindingResult
     * @param modelMap
     * @param request
     * @return
     */
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    String save(@Valid @ModelAttribute("mahasiswa") Mahasiswa mahasiswa, BindingResult bindingResult, ModelMap modelMap,@RequestBody String request){

        if(mahasiswa == null) return "Gagal";
        if(bindingResult.hasErrors()) return "Gagal";

        try{
            // mahasiswaService.save(mahasiswa);
            return "Sukses";
        }catch (HibernateException e){
            return "Gagal";
        }
    }

    @RequestMapping(params = "id", method = RequestMethod.DELETE)
    public @ResponseBody
    String delete(@RequestParam("id") Mahasiswa mahasiswa) {
        try {
            mahasiswaService.delete(mahasiswa);
            return "Sukses";
        }catch (HibernateException e){
            return "Gagal";
        }
    }

    /**
     * kembalikan form edit
     * @param modelMap
     * @param mahasiswa
     * @return
     */
    @RequestMapping(value = "edit", params = "id", method = RequestMethod.GET)
    public String edit(ModelMap modelMap, @RequestParam("id")Mahasiswa mahasiswa) {

        modelMap.addAttribute("event", "EDIT");
        modelMap.addAttribute("httpMethod", "PUT");
        modelMap.addAttribute("mahasiswa", mahasiswa);
        return "mahasiswa/input";
    }

    /**
     * Simpan hasil editan
     * @param mahasiswa
     * @param bindingResult
     * @param modelMap
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT)
    public @ResponseBody
    String update(@Valid @ModelAttribute("mahasiswa") Mahasiswa mahasiswa, BindingResult bindingResult, ModelMap modelMap){

        if(mahasiswa == null) return "Gagal";
        if(bindingResult.hasErrors()) return "Gagal";

        try{
            mahasiswaService.save(mahasiswa);
            return "Sukses";
        }catch (HibernateException e){
            return "Gagal";
        }
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) throws Exception{
        binder.registerCustomEditor(Mahasiswa.class, new MahasiswaEditor(mahasiswaService));
    }
}
