package com.robin.akademik.fakultas.controller.rest;

import com.robin.akademik.core.domain.Dosen;
import com.robin.akademik.core.editor.DosenEditor;
import com.robin.akademik.core.service.DosenService;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Robson
 * Date: 03/04/12
 * Time: 17:30
 * To change this template use File | Settings | File Templates.
 */

@Controller
@RequestMapping(value = "rest/dosen")

public class DosenRestController {
    @Autowired
    @Qualifier("dosenFakultasService")
    private DosenService dosenService;

    /**
     * Ini select * all
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public
    @ResponseBody
    List find() {
        return dosenService.find();
    }

    @RequestMapping(params = "nip", method = RequestMethod.GET)
    public
    @ResponseBody
    Dosen find(@RequestParam("nip") Dosen dosen) {
        return dosen;
    }

    /**
     * Simpan hasil inputan
     *
     * @param dosen
     */
    @RequestMapping(method = RequestMethod.POST)
    public
    @ResponseBody
    String save(@RequestBody Dosen dosen) {

        if (dosen == null) return "Gagal";

        try {
            dosenService.save(dosen);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }

//    /**
//     * Simpan hasil inputan
//     *
//     * @param asuransies
//     */
//    @RequestMapping(value = "list", method = RequestMethod.POST)
//    public
//    @ResponseBody
//    String saves(@RequestBody Dosen [] dosens) {
//
//        if (dosens == null) return "Gagal";
//
//        try {
//            dosenService.save(dosens);
//            return "Sukses";
//        } catch (HibernateException e) {
//            return "Gagal";
//        }
//    }

    @RequestMapping(params = "nip", method = RequestMethod.DELETE)
    public
    @ResponseBody
    String delete(@RequestParam("nip") Dosen dosen) {
        try {
            dosenService.delete(dosen);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }

    /**
     * Simpan hasil editan
     *
     * @param dosen
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT)
    public
    @ResponseBody
    String update(@RequestBody Dosen dosen) {

        if (dosen == null) return "Gagal";

        try {
            dosenService.save(dosen);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }
//    /**
//     * Simpan hasil inputan
//     *
//     * @param dosens
//     */
//    @RequestMapping(value = "list", method = RequestMethod.PUT)
//    public
//    @ResponseBody
//    String updates(@RequestBody Dosen [] dosens) {
//
//        if (dosens == null) return "Gagal";
//
//        try {
//            dosenService.save(dosens);
//            return "Sukses";
//        } catch (HibernateException e) {
//            return "Gagal";
//        }
//    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) throws Exception {
        binder.registerCustomEditor(Dosen.class, new DosenEditor(dosenService));
    }


}
