package com.robin.akademik.fakultas.controller.rest;

import com.robin.akademik.core.domain.Propinsi;
import com.robin.akademik.core.editor.PropinsiEditor;
import com.robin.akademik.core.service.PropinsiService;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Robson
 * Date: 03/04/12
 * Time: 17:30
 * To change this template use File | Settings | File Templates.
 */

@Controller
@RequestMapping(value = "rest/propinsi")

public class PropinsiRestController {
    @Autowired
    @Qualifier("propinsiFakultasService")
    private PropinsiService propinsiService;

    /**
     * Ini select * all
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public
    @ResponseBody
    List find() {
        return propinsiService.find();
    }

    @RequestMapping(params = "namapropinsi", method = RequestMethod.GET)
    public
    @ResponseBody
    Propinsi find(@RequestParam("namapropinsi") Propinsi propinsi) {
        return propinsi;
    }

    /**
     * Simpan hasil inputan
     *
     * @param propinsi
     */
    @RequestMapping(method = RequestMethod.POST)
    public
    @ResponseBody
    String save(@RequestBody Propinsi propinsi) {

        if (propinsi == null) return "Gagal";

        try {
            propinsiService.save(propinsi);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }

//    /**
//     * Simpan hasil inputan
//     *
//     * @param asuransies
//     */
//    @RequestMapping(value = "list", method = RequestMethod.POST)
//    public
//    @ResponseBody
//    String saves(@RequestBody Propinsi [] propinsis) {
//
//        if (propinsis == null) return "Gagal";
//
//        try {
//            propinsiService.save(propinsis);
//            return "Sukses";
//        } catch (HibernateException e) {
//            return "Gagal";
//        }
//    }

    @RequestMapping(params = "namapropinsi", method = RequestMethod.DELETE)
    public
    @ResponseBody
    String delete(@RequestParam("namapropinsi") Propinsi propinsi) {
        try {
            propinsiService.delete(propinsi);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }

    /**
     * Simpan hasil editan
     *
     * @param propinsi
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT)
    public
    @ResponseBody
    String update(@RequestBody Propinsi propinsi) {

        if (propinsi == null) return "Gagal";

        try {
            propinsiService.save(propinsi);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }
//    /**
//     * Simpan hasil inputan
//     *
//     * @param propinsis
//     */
//    @RequestMapping(value = "list", method = RequestMethod.PUT)
//    public
//    @ResponseBody
//    String updates(@RequestBody Propinsi [] propinsis) {
//
//        if (propinsis == null) return "Gagal";
//
//        try {
//            propinsiService.save(propinsis);
//            return "Sukses";
//        } catch (HibernateException e) {
//            return "Gagal";
//        }
//    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) throws Exception {
        binder.registerCustomEditor(Propinsi.class, new PropinsiEditor(propinsiService));
    }


}
