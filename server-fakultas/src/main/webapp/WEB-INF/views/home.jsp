<%--
  Created by IntelliJ IDEA.
  User: Robson
  Date: 20/03/12
  Time: 17:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
    <!-- This template was created by Mantis-a [http://www.mantisa.cz/]. For more templates visit Free website templates [http://www.mantisatemplates.com/]. -->

    <meta name="Description" content="..."/>
    <meta name="Keywords" content="..."/>
    <meta name="robots" content="all,follow"/>
    <meta name="author" content="..."/>
    <meta name="copyright" content="Mantis-a [http://www.mantisa.cz/]"/>

    <meta http-equiv="Content-Script-Type" content="text/javascript"/>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <!-- CSS -->
    <LINK REL=StyleSheet HREF="<c:url value='/resources/style.css' />" TYPE="text/css" MEDIA=screen>
    <LINK REL=StyleSheet HREF="<c:url value='/resources/style-print.css' />" TYPE="text/css" MEDIA=screen>

    <%--<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>--%>

    <title>Universitas Komptek| Yogyakarta</title>
</head>

<body>
<div id="wrapper">

    <!-- Title -->
    <div class="title">
        <div class="title-top">
            <div class="title-left">
                <div class="title-right">
                    <div class="title-bottom">
                        <div class="title-top-left">
                            <div class="title-bottom-left">
                                <div class="title-top-right">
                                    <div class="title-bottom-right">
                                        <h1><span>Universitas Komptek</span></h1>

                                        <p>Yogyakarta</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Title end -->

    <hr class="noscreen"/>

    <div class="content">

        <div class="column-left">
            <h3><font color="Yellow">MENU</font></h3>
            <a href="#skip-menu" class="hidden">Skip menu</a>
            <ul class="menu">
                <li><a href="/" class="active">Fakultas</a></li>
                <li><a href="#">Jurusan</a></li>
                <li><a href="#">Propinsi</a></li>
                <li><a href="#">Matakuliah</a></li>
                <li><a href="#">Dosen</a></li>
                <li><a href="#">Mahasiswa</a></li>
                <li><a href="#">Kuliah</a></li>
                <li><a href="#">Nilai</a></li>
                <li class="last"><a href="#">LogOut</a></li>
            </ul>
        </div>

        <div id="skip-menu"></div>
        <div class="column-right">
            <div class="box">
                <div class="box-top"></div>
                <div class="box-in">
                    <h2>DATA FAKULTAS</h2>


                </div>
            </div>

            <div class="box-bottom">

                <hr class="noscreen"/>

                <div class="footer-info-left">Robynson, Vivi, Sumarni</div>
                <div class="footer-info-right">MMI UGM 2011</div>
            </div>
        </div>

        <div class="cleaner">&nbsp;</div>
    </div>
</div>
</body>
</html>
