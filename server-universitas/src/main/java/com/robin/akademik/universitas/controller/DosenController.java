package com.robin.akademik.universitas.controller;

import com.robin.akademik.core.domain.Dosen;
import com.robin.akademik.core.editor.DosenEditor;
import com.robin.akademik.core.service.DosenService;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by IntelliJ IDEA.
 * User: Robson
 * Date: 27/03/12
 * Time: 16:39
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "dosen")
public class DosenController {

    @Autowired
    @Qualifier("dosenUniversitasService")
    private DosenService dosenService;

    @RequestMapping(value = "navbar", method = RequestMethod.GET)
    public String navBar() {
        return "dosen/navbar";
    }
     /**
     * Ini select * all
     * @param modelMap
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public String find(ModelMap modelMap) {
        modelMap.addAttribute("dosens", dosenService.find());
        return "dosen/list";
    }

    @RequestMapping(params = "nip", method = RequestMethod.GET)
    public String find(ModelMap modelMap, @RequestParam("nip") Dosen dosen) {
        modelMap.addAttribute("event", "DETAIL");
        modelMap.addAttribute("dosen", dosen);
        return "dosen/input";
    }

    /**
     * kembalikan form inputan
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "new", method = RequestMethod.GET)
    public String create(ModelMap modelMap) {
        modelMap.addAttribute("event", "INPUT");
        modelMap.addAttribute("httpMethod", "POST");
        modelMap.addAttribute("dosen", new Dosen());
        return "dosen/input";
    }

    /***
     * Simpan hasil inputan
     * @param dosen
     * @param bindingResult
     * @param modelMap
     * @param request
     * @return
     */
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    String save(@Valid @ModelAttribute("dosen") Dosen dosen, BindingResult bindingResult, ModelMap modelMap,@RequestBody String request){

        if(dosen == null) return "Gagal";
        if(bindingResult.hasErrors()) return "Gagal";

        try{
            dosenService.save(dosen);
            return "Sukses";

        }catch (HibernateException e){
            return "Gagal";
        }
    }

    @RequestMapping(params = "nip", method = RequestMethod.DELETE)
    public @ResponseBody
    String delete(@RequestParam("nip") Dosen dosen) {
        try {
            dosenService.delete(dosen);
            return "Sukses";
        }catch (HibernateException e){
            return "Gagal";
        }
    }

    /**
     * kembalikan form edit
     * @param modelMap
     * @param dosen
     * @return
     */
    @RequestMapping(value = "edit", params = "nip", method = RequestMethod.GET)
    public String edit(ModelMap modelMap, @RequestParam("nip")Dosen dosen) {

        modelMap.addAttribute("event", "EDIT");
        modelMap.addAttribute("httpMethod", "PUT");
        modelMap.addAttribute("dosen", dosen);
        return "dosen/input";
    }

    /**
     * Simpan hasil editan
     * @param dosen
     * @param bindingResult
     * @param modelMap
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT)
    public @ResponseBody
    String update(@Valid @ModelAttribute("dosen") Dosen dosen, BindingResult bindingResult, ModelMap modelMap){

        if(dosen == null) return "Gagal";
        if(bindingResult.hasErrors()) return "Gagal";

        try{
            dosenService.save(dosen);
            return "Sukses";
        }catch (HibernateException e){
            return "Gagal";
        }
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) throws Exception{
        binder.registerCustomEditor(Dosen.class, new DosenEditor(dosenService));
    }
}
