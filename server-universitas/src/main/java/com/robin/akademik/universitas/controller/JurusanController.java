package com.robin.akademik.universitas.controller;

import com.robin.akademik.core.domain.Jurusan;
import com.robin.akademik.core.editor.JurusanEditor;
import com.robin.akademik.core.service.JurusanService;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by IntelliJ IDEA.
 * User: Robson
 * Date: 27/03/12
 * Time: 16:39
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "jurusan")
public class JurusanController {

    @Autowired
    @Qualifier("jurusanUniversitasService")
    private JurusanService jurusanService;

    /**
     * Ini select * all
     * @param modelMap
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public String find(ModelMap modelMap) {
        modelMap.addAttribute("jurusan", jurusanService.find());
        return "jurusan/list";
    }

    @RequestMapping(params = "nip", method = RequestMethod.GET)
    public String find(ModelMap modelMap, @RequestParam("nip") Jurusan jurusan) {
        modelMap.addAttribute("event", "DETAIL");
        modelMap.addAttribute("jurusan", jurusan);
        return "jurusan/input";
    }

    /**
     * kembalikan form inputan
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "new", method = RequestMethod.GET)
    public String create(ModelMap modelMap) {
        modelMap.addAttribute("event", "INPUT");
        modelMap.addAttribute("httpMethod", "POST");
        modelMap.addAttribute("jurusan", new Jurusan());
        return "jurusan/input";
    }

    /***
     * Simpan hasil inputan
     * @param jurusan
     * @param bindingResult
     * @param modelMap
     * @param request
     * @return
     */
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    String save(@Valid @ModelAttribute("jurusan") Jurusan jurusan, BindingResult bindingResult, ModelMap modelMap,@RequestBody String request){

        if(jurusan == null) return "Gagal";
        if(bindingResult.hasErrors()) return "Gagal";

        try{
            // jurusanService.save(jurusan);
            return "Sukses";
        }catch (HibernateException e){
            return "Gagal";
        }
    }

    @RequestMapping(params = "id", method = RequestMethod.DELETE)
    public @ResponseBody
    String delete(@RequestParam("id") Jurusan jurusan) {
        try {
            jurusanService.delete(jurusan);
            return "Sukses";
        }catch (HibernateException e){
            return "Gagal";
        }
    }

    /**
     * kembalikan form edit
     * @param modelMap
     * @param jurusan
     * @return
     */
    @RequestMapping(value = "edit", params = "id", method = RequestMethod.GET)
    public String edit(ModelMap modelMap, @RequestParam("id")Jurusan jurusan) {

        modelMap.addAttribute("event", "EDIT");
        modelMap.addAttribute("httpMethod", "PUT");
        modelMap.addAttribute("jurusan", jurusan);
        return "jurusan/input";
    }

    /**
     * Simpan hasil editan
     * @param jurusan
     * @param bindingResult
     * @param modelMap
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT)
    public @ResponseBody
    String update(@Valid @ModelAttribute("jurusan") Jurusan jurusan, BindingResult bindingResult, ModelMap modelMap){

        if(jurusan == null) return "Gagal";
        if(bindingResult.hasErrors()) return "Gagal";

        try{
            jurusanService.save(jurusan);
            return "Sukses";
        }catch (HibernateException e){
            return "Gagal";
        }
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) throws Exception{
        binder.registerCustomEditor(Jurusan.class, new JurusanEditor(jurusanService));
    }
}
