package com.robin.akademik.universitas.controller;

import com.robin.akademik.core.domain.Kuliah;
import com.robin.akademik.core.editor.KuliahEditor;
import com.robin.akademik.core.service.KuliahService;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by IntelliJ IDEA.
 * User: Robson
 * Date: 27/03/12
 * Time: 16:39
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "kuliah")
public class KuliahController {

    @Autowired
    @Qualifier("kuliahUniversitasService")
    private KuliahService kuliahService;

    /**
     * Ini select * all
     * @param modelMap
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public String find(ModelMap modelMap) {
        modelMap.addAttribute("kuliah", kuliahService.find());
        return "kuliah/list";
    }

    @RequestMapping(params = "nip", method = RequestMethod.GET)
    public String find(ModelMap modelMap, @RequestParam("nip") Kuliah kuliah) {
        modelMap.addAttribute("event", "DETAIL");
        modelMap.addAttribute("kuliah", kuliah);
        return "kuliah/input";
    }

    /**
     * kembalikan form inputan
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "new", method = RequestMethod.GET)
    public String create(ModelMap modelMap) {
        modelMap.addAttribute("event", "INPUT");
        modelMap.addAttribute("httpMethod", "POST");
        modelMap.addAttribute("kuliah", new Kuliah());
        return "kuliah/input";
    }

    /***
     * Simpan hasil inputan
     * @param kuliah
     * @param bindingResult
     * @param modelMap
     * @param request
     * @return
     */
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    String save(@Valid @ModelAttribute("kuliah") Kuliah kuliah, BindingResult bindingResult, ModelMap modelMap,@RequestBody String request){

        if(kuliah == null) return "Gagal";
        if(bindingResult.hasErrors()) return "Gagal";

        try{
            // kuliahService.save(kuliah);
            return "Sukses";
        }catch (HibernateException e){
            return "Gagal";
        }
    }

    @RequestMapping(params = "id", method = RequestMethod.DELETE)
    public @ResponseBody
    String delete(@RequestParam("id") Kuliah kuliah) {
        try {
            kuliahService.delete(kuliah);
            return "Sukses";
        }catch (HibernateException e){
            return "Gagal";
        }
    }

    /**
     * kembalikan form edit
     * @param modelMap
     * @param kuliah
     * @return
     */
    @RequestMapping(value = "edit", params = "id", method = RequestMethod.GET)
    public String edit(ModelMap modelMap, @RequestParam("id")Kuliah kuliah) {

        modelMap.addAttribute("event", "EDIT");
        modelMap.addAttribute("httpMethod", "PUT");
        modelMap.addAttribute("kuliah", kuliah);
        return "kuliah/input";
    }

    /**
     * Simpan hasil editan
     * @param kuliah
     * @param bindingResult
     * @param modelMap
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT)
    public @ResponseBody
    String update(@Valid @ModelAttribute("kuliah") Kuliah kuliah, BindingResult bindingResult, ModelMap modelMap){

        if(kuliah == null) return "Gagal";
        if(bindingResult.hasErrors()) return "Gagal";

        try{
            kuliahService.save(kuliah);
            return "Sukses";
        }catch (HibernateException e){
            return "Gagal";
        }
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) throws Exception{
        binder.registerCustomEditor(Kuliah.class, new KuliahEditor(kuliahService));
    }
}
