package com.robin.akademik.universitas.controller;

import com.robin.akademik.core.domain.Matakuliah;
import com.robin.akademik.core.editor.MatakuliahEditor;
import com.robin.akademik.core.service.MatakuliahService;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by IntelliJ IDEA.
 * User: Robson
 * Date: 27/03/12
 * Time: 16:39
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "matakuliah")
public class MatakuliahController {

    @Autowired
    @Qualifier("matakuliahUniversitasService")
    private MatakuliahService matakuliahService;

    /**
     * Ini select * all
     * @param modelMap
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public String find(ModelMap modelMap) {
        modelMap.addAttribute("matakuliah", matakuliahService.find());
        return "matakuliah/list";
    }

    @RequestMapping(params = "nip", method = RequestMethod.GET)
    public String find(ModelMap modelMap, @RequestParam("nip") Matakuliah matakuliah) {
        modelMap.addAttribute("event", "DETAIL");
        modelMap.addAttribute("matakuliah", matakuliah);
        return "matakuliah/input";
    }

    /**
     * kembalikan form inputan
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "new", method = RequestMethod.GET)
    public String create(ModelMap modelMap) {
        modelMap.addAttribute("event", "INPUT");
        modelMap.addAttribute("httpMethod", "POST");
        modelMap.addAttribute("matakuliah", new Matakuliah());
        return "matakuliah/input";
    }

    /***
     * Simpan hasil inputan
     * @param matakuliah
     * @param bindingResult
     * @param modelMap
     * @param request
     * @return
     */
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    String save(@Valid @ModelAttribute("matakuliah") Matakuliah matakuliah, BindingResult bindingResult, ModelMap modelMap,@RequestBody String request){

        if(matakuliah == null) return "Gagal";
        if(bindingResult.hasErrors()) return "Gagal";

        try{
            // matakuliahService.save(matakuliah);
            return "Sukses";
        }catch (HibernateException e){
            return "Gagal";
        }
    }

    @RequestMapping(params = "id", method = RequestMethod.DELETE)
    public @ResponseBody
    String delete(@RequestParam("id") Matakuliah matakuliah) {
        try {
            matakuliahService.delete(matakuliah);
            return "Sukses";
        }catch (HibernateException e){
            return "Gagal";
        }
    }

    /**
     * kembalikan form edit
     * @param modelMap
     * @param matakuliah
     * @return
     */
    @RequestMapping(value = "edit", params = "id", method = RequestMethod.GET)
    public String edit(ModelMap modelMap, @RequestParam("id")Matakuliah matakuliah) {

        modelMap.addAttribute("event", "EDIT");
        modelMap.addAttribute("httpMethod", "PUT");
        modelMap.addAttribute("matakuliah", matakuliah);
        return "matakuliah/input";
    }

    /**
     * Simpan hasil editan
     * @param matakuliah
     * @param bindingResult
     * @param modelMap
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT)
    public @ResponseBody
    String update(@Valid @ModelAttribute("matakuliah") Matakuliah matakuliah, BindingResult bindingResult, ModelMap modelMap){

        if(matakuliah == null) return "Gagal";
        if(bindingResult.hasErrors()) return "Gagal";

        try{
            matakuliahService.save(matakuliah);
            return "Sukses";
        }catch (HibernateException e){
            return "Gagal";
        }
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) throws Exception{
        binder.registerCustomEditor(Matakuliah.class, new MatakuliahEditor(matakuliahService));
    }
}
