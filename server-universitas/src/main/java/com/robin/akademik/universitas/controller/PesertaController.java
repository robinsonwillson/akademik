package com.robin.akademik.universitas.controller;

import com.robin.akademik.core.domain.Fakultas;
import com.robin.akademik.core.editor.FakultasEditor;
import com.robin.akademik.core.service.FakultasService;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by IntelliJ IDEA.
 * User: Robson
 * Date: 27/03/12
 * Time: 16:39
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "peserta")
public class PesertaController  {

    @Autowired
    @Qualifier("fakultasUniversitasService")
    private FakultasService fakultasService;

    /**
     * Ini select * all
     * @param modelMap
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public String find(ModelMap modelMap) {
        modelMap.addAttribute("fakultas", fakultasService.find());
        return "fakultas/list";
    }

    @RequestMapping(params = "nip", method = RequestMethod.GET)
    public String find(ModelMap modelMap, @RequestParam("nip") Fakultas fakultas) {
        modelMap.addAttribute("event", "DETAIL");
        modelMap.addAttribute("fakultas", fakultas);
        return "fakultas/input";
    }

    /**
     * kembalikan form inputan
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "new", method = RequestMethod.GET)
    public String create(ModelMap modelMap) {
        modelMap.addAttribute("event", "INPUT");
        modelMap.addAttribute("httpMethod", "POST");
        modelMap.addAttribute("fakultas", new Fakultas());
        return "fakultas/input";
    }

    /***
     * Simpan hasil inputan
     * @param fakultas
     * @param bindingResult
     * @param modelMap
     * @param request
     * @return
     */
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    String save(@Valid @ModelAttribute("fakultas") Fakultas fakultas, BindingResult bindingResult, ModelMap modelMap,@RequestBody String request){

        if(fakultas == null) return "Gagal";
        if(bindingResult.hasErrors()) return "Gagal";

        try{
            // fakultasService.save(fakultas);
            return "Sukses";
        }catch (HibernateException e){
            return "Gagal";
        }
    }

    @RequestMapping(params = "id", method = RequestMethod.DELETE)
    public @ResponseBody
    String delete(@RequestParam("id") Fakultas fakultas) {
        try {
            fakultasService.delete(fakultas);
            return "Sukses";
        }catch (HibernateException e){
            return "Gagal";
        }
    }

    /**
     * kembalikan form edit
     * @param modelMap
     * @param fakultas
     * @return
     */
    @RequestMapping(value = "edit", params = "id", method = RequestMethod.GET)
    public String edit(ModelMap modelMap, @RequestParam("id")Fakultas fakultas) {

        modelMap.addAttribute("event", "EDIT");
        modelMap.addAttribute("httpMethod", "PUT");
        modelMap.addAttribute("fakultas", fakultas);
        return "fakultas/input";
    }

    /**
     * Simpan hasil editan
     * @param fakultas
     * @param bindingResult
     * @param modelMap
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT)
    public @ResponseBody
    String update(@Valid @ModelAttribute("fakultas") Fakultas fakultas, BindingResult bindingResult, ModelMap modelMap){

        if(fakultas == null) return "Gagal";
        if(bindingResult.hasErrors()) return "Gagal";

        try{
            fakultasService.save(fakultas);
            return "Sukses";
        }catch (HibernateException e){
            return "Gagal";
        }
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) throws Exception{
        binder.registerCustomEditor(Fakultas.class, new FakultasEditor(fakultasService));
    }
}
