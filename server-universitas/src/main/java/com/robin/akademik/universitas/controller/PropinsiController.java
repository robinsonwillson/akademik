package com.robin.akademik.universitas.controller;

import com.robin.akademik.core.domain.Propinsi;
import com.robin.akademik.core.editor.PropinsiEditor;
import com.robin.akademik.core.service.PropinsiService;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by IntelliJ IDEA.
 * User: Robson
 * Date: 27/03/12
 * Time: 16:39
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "propinsi")
public class PropinsiController {

    @Autowired
    @Qualifier("propinsiUniversitasService")
    private PropinsiService propinsiService;

    /**
     * Ini select * all
     * @param modelMap
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public String find(ModelMap modelMap) {
        modelMap.addAttribute("propinsi", propinsiService.find());
        return "propinsi/list";
    }

    @RequestMapping(params = "nip", method = RequestMethod.GET)
    public String find(ModelMap modelMap, @RequestParam("nip") Propinsi propinsi) {
        modelMap.addAttribute("event", "DETAIL");
        modelMap.addAttribute("propinsi", propinsi);
        return "propinsi/input";
    }

    /**
     * kembalikan form inputan
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "new", method = RequestMethod.GET)
    public String create(ModelMap modelMap) {
        modelMap.addAttribute("event", "INPUT");
        modelMap.addAttribute("httpMethod", "POST");
        modelMap.addAttribute("propinsi", new Propinsi());
        return "propinsi/input";
    }

    /***
     * Simpan hasil inputan
     * @param propinsi
     * @param bindingResult
     * @param modelMap
     * @param request
     * @return
     */
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    String save(@Valid @ModelAttribute("propinsi") Propinsi propinsi, BindingResult bindingResult, ModelMap modelMap,@RequestBody String request){

        if(propinsi == null) return "Gagal";
        if(bindingResult.hasErrors()) return "Gagal";

        try{
            // propinsiService.save(propinsi);
            return "Sukses";
        }catch (HibernateException e){
            return "Gagal";
        }
    }

    @RequestMapping(params = "id", method = RequestMethod.DELETE)
    public @ResponseBody
    String delete(@RequestParam("id") Propinsi propinsi) {
        try {
            propinsiService.delete(propinsi);
            return "Sukses";
        }catch (HibernateException e){
            return "Gagal";
        }
    }

    /**
     * kembalikan form edit
     * @param modelMap
     * @param propinsi
     * @return
     */
    @RequestMapping(value = "edit", params = "id", method = RequestMethod.GET)
    public String edit(ModelMap modelMap, @RequestParam("id")Propinsi propinsi) {

        modelMap.addAttribute("event", "EDIT");
        modelMap.addAttribute("httpMethod", "PUT");
        modelMap.addAttribute("propinsi", propinsi);
        return "propinsi/input";
    }

    /**
     * Simpan hasil editan
     * @param propinsi
     * @param bindingResult
     * @param modelMap
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT)
    public @ResponseBody
    String update(@Valid @ModelAttribute("propinsi") Propinsi propinsi, BindingResult bindingResult, ModelMap modelMap){

        if(propinsi == null) return "Gagal";
        if(bindingResult.hasErrors()) return "Gagal";

        try{
            propinsiService.save(propinsi);
            return "Sukses";
        }catch (HibernateException e){
            return "Gagal";
        }
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) throws Exception{
        binder.registerCustomEditor(Propinsi.class, new PropinsiEditor(propinsiService));
    }
}
