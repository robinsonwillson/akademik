package com.robin.akademik.universitas.service;

import com.robin.akademik.core.domain.Dosen;
import com.robin.akademik.core.service.impl.DosenServiceImpl;
import com.robin.akademik.core.service.impl.DosenServiceImpl;
import com.robin.akademik.core.util.system.SystemUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

/**
 * Created by IntelliJ IDEA.
 * User: Robson
 * Date: 03/04/12
 * Time: 16:59
 * To change this template use File | Settings | File Templates.
 */
@Transactional
@Service("dosenUniversitasService")
public class DosenServiceUniversitasImpl extends DosenServiceImpl{
    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;


    @Override
    public void save(Dosen dosen) {
        HttpEntity<Dosen> httpEntity = new HttpEntity<Dosen>(dosen);
        ResponseEntity<String> responseEntity = restTemplate.exchange(SystemUtils.HOST_WS_FAKULTAS + "/dosen", HttpMethod.POST, httpEntity, String.class);
        super.save(dosen);
    }

    @Override
    public void delete(Dosen dosen) {
        super.delete(dosen);
    }

}