package com.robin.akademik.universitas.service;

import com.robin.akademik.core.service.impl.DosenServiceImpl;
import com.robin.akademik.core.service.impl.MahasiswaServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by IntelliJ IDEA.
 * User: Robson
 * Date: 03/04/12
 * Time: 16:59
 * To change this template use File | Settings | File Templates.
 */
@Transactional
@Service("mahsiswaUniversitasService")
public class MahasiswaServiceUniversitasImpl extends MahasiswaServiceImpl{
}
