package com.robin.akademik.universitas.service;

import com.robin.akademik.core.domain.Propinsi;
import com.robin.akademik.core.service.impl.PropinsiServiceImpl;
import com.robin.akademik.core.util.system.SystemUtils;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

/**
 * Created by IntelliJ IDEA.
 * User: Robson
 * Date: 03/04/12
 * Time: 16:59
 * To change this template use File | Settings | File Templates.
 */

@Transactional
@Service("propinsiUniversitasService")
public class PropinsiServiceUniversitasImpl extends PropinsiServiceImpl{

    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;


    @Override
    public void save(Propinsi propinsi) {
        HttpEntity<Propinsi> httpEntity = new HttpEntity<Propinsi>(propinsi);
        ResponseEntity<String> responseEntity = restTemplate.exchange(SystemUtils.HOST_WS_FAKULTAS + "/propinsi", HttpMethod.POST, httpEntity, String.class);
        super.save(propinsi);
    }

    @Override
    public void delete(Propinsi propinsi) {
        super.delete(propinsi);
    }
}
