<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%--<c:set var="enabledInput" value="${event == 'DETAIL'}" />--%>
<c:url value="/dosen" var="dosen_url"/>

<form:form id="formInputDosen" class="modal-form" action="${dosen_url}" method="${httpMethod}"
           modelAttribute="dosen">
    <table>

        <tr>
            <td>
                <label for="textNip">NIP</label>
            </td>
            <td>
                <form:input path="nip" id="textNip" class="input-xlarge" disabled="${enabledInput}"/>
            </td>
        </tr>

        <tr>
            <td>
                <label for="textNama">Nama</label>
            </td>
            <td>
                <form:input path="nama" id="textNama" class="input-xlarge" disabled="${enabledInput}"/>
                </div>
            </td>
        </tr>

            <%--Alamat--%>
        <tr>
            <td valign="top">
                <label for="textAlamat" class="input-xlarge">Alamat</label>
            </td>
            <td>
                <form:textarea path="alamat" id="textAlamat" rows="3" class="input-xlarge" disabled="${enabledInput}"/>
            </td>
        </tr>

        <tr>
            <td>
                <label for="textTelp">Telp</label>
            </td>
            <td>
                <form:input path="telepon" id="textTelp" class="input-xlarge" disabled="${enabledInput}"/>
                </div>
            </td>
        </tr>
    </table>

    <input type="submit" value="Simpan"/>
    <input type="reset" value="Batal">
</form:form>

<script type="text/javascript" src="<c:url value='/resources/js/form.js'/>"></script>
