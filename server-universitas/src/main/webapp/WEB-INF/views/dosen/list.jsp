<%--
  Created by IntelliJ IDEA.
  User: Robson
  Date: 28/03/12
  Time: 17:28
  To change this template use File | Settings | File Templates.
--%>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<LINK REL=StyleSheet HREF="<c:url value='/resources/style.css' />" TYPE="text/css" MEDIA=screen>
<LINK REL=StyleSheet HREF="<c:url value='/resources/style1.css' />" TYPE="text/css" MEDIA=screen>

<c:url value="/dosen" var="dosenUrlDelete"/>
<c:url value="/dosen/edit" var="dosenUrlEdit"/>

<table class="imagetable">
    <thead>
    <tr>
        <th align=center>NIP</th>
        <th align=center>NAMA</th>
        <th align=center>ALAMAT</th>
        <th align=center>TELEPON</th>
        <th align=center>ACTION</th>
    </tr>
    </thead>
    <tbody>
        <c:forEach items="${dosens}"  var="dosen">
            <tr>
                <td>${dosen.nip}</td>
                <td>${dosen.nama}</td>
                <td>${dosen.alamat}</td>
                <td>${dosen.telepon}</td>
                <td>
                    <div>
                        <a href="#"
                           onclick="buttonDeleteClick('${dosenUrlDelete}','${dosen.nip}')">hapus</a>

                        &nbsp;  |  &nbsp;

                        <a href="#"
                           onclick="buttonEditClick('${dosenUrlEdit}','${dosen.nip}')">Ubah</a>
                    </div>
                </td>

            </tr>
        </c:forEach>
    </tbody>
</table>


<script type="text/javascript" src="<c:url value='/resources/js/jquery.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/menu.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/delete.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/edit.js'/>"></script>

