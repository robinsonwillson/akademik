<%--
  Created by IntelliJ IDEA.
  User: Robson
  Date: 20/03/12
  Time: 17:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
    <title>COMPANY NAME</title>
    <LINK REL=StyleSheet HREF="<c:url value='/resources/style.css' />" TYPE="text/css" MEDIA=screen>
    <LINK REL=StyleSheet HREF="<c:url value='/resources/style1.css' />" TYPE="text/css" MEDIA=screen>
</head>

<body>
<div id="page">
    <div id="header">
        <h1>Universitas Komptek</h1>

        <div class="description">Yogyakarta</div>
    </div>

    <div id="mainarea">

        <div id="sidebar">
            <div class="sidebarheader">Menu</div>
            <div id="sidebarnav">
                <a id="menupropinsi" href="#">Propinsi</a> <br>
                <a id="menujurusan" href="#">Jurusan</a> <br>
                <a id="menudosen" href="#">Dosen</a> <br>
                <a id="menumatakuliah" href="#">Matakuliah</a> <br>
                <a id="menumahasiswa" href="#">Mahasiswa</a> <br>
                <a id="menukuliah" href="#">Kuliah</a> <br>
                <a id="menunilai" href="#">Nilai</a> <br>
                <a id="menulogout" href="#">Logout</a>

            </div>
        </div>


        <div id="sidebar1">
            <div id="tampilandata">

                <div id="navbar">

                </div>

                <div id="list">

                </div>

                <div id="input">

                </div>

            </div>

        </div>
    </div>

    <div id="footer">
        Copyright @ 2012 by Robinson, Vivie and Sumarni
    </div>


</div>

<script type="text/javascript" src="<c:url value='/resources/js/jquery.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/menu.js'/>"></script>

</body>

</html>