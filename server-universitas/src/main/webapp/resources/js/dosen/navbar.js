/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/19/12
 * Time: 12:23 PM
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function () {

    var buttonAddDosen = $('#adddosen');
    var buttonRefreshDosen = $('#reloaddosen');

    buttonAddDosen.click(function () {
        $('#list').html("");
        $.get('/universitas/dosen/new', function (data) {
            $('#input').html(data);
        });

    });

    reload = function () {
        $.ajax({
            url:'/universitas/dosen',
            type:'GET',
            success:function (result) {
                $('#list').html(result);

            },
            error:function (xhr, status, error) {
                alert(status);
            }
        });
    };

    buttonRefreshDosen.click(function () {
        $.get('/universitas/dosen', function (data){
            $('#list').html(data);
        });
    });

});

