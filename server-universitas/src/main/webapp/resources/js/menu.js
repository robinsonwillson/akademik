/**
 * Created by IntelliJ IDEA.
 * User: Robson
 * Date: 28/03/12
 * Time: 17:03
 * To change this template use File | Settings | File Templates.
 */


$(function(){

    var menuPropinsi = $('#menupropinsi');
    var menuJurusan = $('#menujurusan');
    var menuDosen = $('#menudosen');
    var menuMatakuliah = $('#menumatakuliah');
    var menuMahasiswa = $('#menumahasiswa');
    var menuKuliah = $('#menukuliah');
    var menuNilai = $('#menunilai');
    var menuLogout = $('#menulogout');

    menuMahasiswa.click(function(){
        $.get('/universitas/mahasiswa/navbar', function (data){
            $('#navbar').html(data);
        });
        $.get('/universitas/mahasiswa', function (data){
            $('#list').html(data);
        });
        $('#input').html("");
    });

    menuDosen.click(function(){
        $.get('/universitas/dosen/navbar', function (data){
            $('#navbar').html(data);
        });
        $.get('/universitas/dosen', function (data){
            $('#list').html(data);
        });
        $('#input').html("");
    });
});

var reload=function(){};
